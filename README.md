# Fonctionnalités

Reprise du projet Jax-RS et intégration de la persistence en base via l'implémentation de JPA de TomEE

## Note

Etant donné qu'on a repris notre propre code, il y a juste une méthode qui manque par rapport à votre code (obtenir un adhérent grâce à son ID).
On n'a pas jugé cela nécessaire en l'état étant donné que le but de l'exercice est surtout de bien utiliser JPA, mais on peut le rajouter si vous voulez.