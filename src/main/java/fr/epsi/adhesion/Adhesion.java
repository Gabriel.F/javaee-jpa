package fr.epsi.adhesion;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Adherent")
public class Adhesion {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Basic
	@Column(length = 200, nullable=false)
	private String email;

	@Basic
	@Column(length = 200, nullable=false)
	private String motDePasse;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date dateAdhesion;

    @Transient
	private boolean conditionsAcceptees;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public boolean isConditionsAcceptees() {
		return conditionsAcceptees;
	}

	public void setConditionsAcceptees(boolean conditionsAcceptees) {
		this.conditionsAcceptees = conditionsAcceptees;
	}

	public Date getDateAdhesion() {
		return dateAdhesion;
	}

	public void setDateAdhesion(Date dateAdhesion) {
		this.dateAdhesion = dateAdhesion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void valider(String confirmationMotDePasse) throws ValidationException {
		List<String> raisons = new ArrayList<>();
		
		if (this.email == null || "".equals(this.email)) {
			raisons.add("adhesion.email.vide");
		}
		else if (! this.email.contains("@")) {
			raisons.add("adhesion.email.invalide");
		}

		if (this.motDePasse == null || this.motDePasse.length() < 8) {
			raisons.add("adhesion.motDePasse.tropCourt");
		}
		
		if (confirmationMotDePasse == null || ! confirmationMotDePasse.equals(this.motDePasse)) {
			raisons.add("adhesion.motDePasse.confirmationIncorrecte");
		}

		if (! this.conditionsAcceptees) {
			raisons.add("adhesion.conditionsNonAcceptees");
		}
		
		if (! raisons.isEmpty()) {
			throw new ValidationException(raisons);
		}
		
		this.dateAdhesion = new Date();
	}

}
